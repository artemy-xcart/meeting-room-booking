## Meeting room booking

### Requirements: 
* *PHP 7+*
* *PostgreSQL* or another database compatible with *ORM Doctrine*
* *NPM* (compilation scss and js files)
* *Composer*

### Optional
* *Docker* (you can up the development environment by *Docker*)

### Also, using
* *Symfony*
* *Twig*
* and other free libraries  

### Installation
* If you have dev environment you need to set up .env and run command 
```
sh ./install_without_docker.sh
```

* If you want to up dev environment and have Docker you can run command
```
sh ./install_with_docker.sh
```

### Demo
![text](https://i.ibb.co/xjyg2gw/1.png){width=800}

![text](https://i.ibb.co/ynFK2ss/2.png){width=800}

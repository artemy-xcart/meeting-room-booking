#!/bin/sh

echo "Installation script starting"

composer install && \
npm install && \
npm run build && \
rm -rf ./.env.local && \
bin/console doctrine:schema:drop --force && \
bin/console doctrine:schema:create && \
bin/console doctrine:fixtures:load -n && \
echo 'APP_ENV=prod' > ./.env.local && \
bin/console cache:clear -n && \

echo "Installation script finished"
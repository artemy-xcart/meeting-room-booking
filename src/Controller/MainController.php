<?php

namespace App\Controller;

use App\Entity\Event;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="app_main")
     */
    public function index(EntityManagerInterface $em): Response
    {
        $repo   = $em->getRepository(Event::class);
        $events = $repo->findAll();

        return $this->render('main/index.html.twig', [
            'events' => $this->mapEvents($events),
        ]);
    }

    protected function mapEvents(array $events): array
    {
        return array_map(static fn($event) => [
            'id'          => $event->getId(),
            'start'       => $event->getStartTime() * 1000,
            'end'         => $event->getEndTime() * 1000,
            'username'    => $event->getUser()->getUserIdentifier(),
            'description' => $event->getDescription(),
        ], $events);
    }

    /**
     * @Route("/event", methods={"POST"})
     */
    public function event(Request $request, EntityManagerInterface $em): Response
    {
        $user  = $this->getUser();
        $error = null;

        if ($user) {
            $repo = $em->getRepository(Event::class);

            $start = $request->request->get('start') / 1000;
            $end = $request->request->get('end') / 1000;

            $criteria = new Criteria();
            $criteria->where(
                Criteria::expr()->orX(
                    Criteria::expr()->andX(
                        Criteria::expr()->lt('startTime', $start),
                        Criteria::expr()->gt('endTime', $start)
                    ),
                    Criteria::expr()->andX(
                        Criteria::expr()->lt('startTime', $end),
                        Criteria::expr()->gt('endTime', $end)
                    )
                )
            );

            if ($repo->matching($criteria)->count() === 0) {
                $event = new Event();
                $event->setStartTime($start);
                $event->setEndTime($end);
                $event->setUser($user);
                $event->setDescription($request->request->get('title'));

                $em->persist($event);
                $em->flush();

            } else {
                $error = 'Event already exists';
            }

        } else {
            $error = 'User is not found';
        }

        return (new JsonResponse())->setData([
            'response' => $error ? 'error' : 'success',
            'error'    => $error
        ]);
    }
}

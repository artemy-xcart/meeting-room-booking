<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Event;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        $user = $this->createTestUser($manager);
        $this->createTestEvents($user, $manager);
    }

    protected function createTestUser(ObjectManager $manager): User
    {
        $user = new User();
        $user->setUsername('admin');

        $user->setRoles([
            'ROLE_ADMIN'
        ]);

        $password = $this->hasher->hashPassword($user, '123456');
        $user->setPassword($password);

        $manager->persist($user);
        $manager->flush();

        return $user;
    }

    protected function createTestEvents(User $user, ObjectManager $manager): void
    {
        $items = [
            [
                'description' => 'Test Event 1',
                'start' => 1649041200,
                'end' => 1649048400,
            ],
            [
                'description' => 'Test Event 2',
                'start' => 1649140200,
                'end' => 1649151000,
            ],
            [
                'description' => 'Test Event 3',
                'start' => 1649386800,
                'end' => 1649395800,
            ],
            [
                'description' => 'Test Event 4',
                'start' => 1649215800,
                'end' => 1649226600,
            ],
            [
                'description' => 'Test Event 5',
                'start' => 1649395800,
                'end' => 1649404800,
            ],
            [
                'description' => 'Test Event 6',
                'start' => 1649340000,
                'end' => 1649354400,
            ],
            [
                'description' => 'Test Event 7',
                'start' => 1649478600,
                'end' => 1649530800,
            ],
            [
                'description' => 'Test Event 8',
                'start' => 1649545200,
                'end' => 1649547000,
            ]
        ];

        foreach ($items as $item) {
            $event = new Event();
            $event
                ->setUser($user)
                ->setDescription($item['description'])
                ->setStartTime($item['start'])
                ->setEndTime($item['end']);
            $manager->persist($event);
        }

        $manager->flush();
    }
}

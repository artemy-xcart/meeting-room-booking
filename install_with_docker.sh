#!/bin/sh

echo "Installation script starting"

docker-compose create psql_server  && \
docker-compose start psql_server && \
docker-compose create web_server && \
docker-compose start web_server && \
composer install && \
npm install && \
npm run build && \
rm -rf ./.env.local && \
bin/console doctrine:schema:drop --force && \
bin/console doctrine:schema:create && \
bin/console doctrine:fixtures:load -n && \
echo 'DATABASE_URL="postgresql://$DB_USER:$DB_PASSWORD@psql_server:$DB_PORT/$DB_NAME?serverVersion=13&charset=utf8"\nAPP_ENV=prod' > ./.env.local && \
bin/console cache:clear && \

echo "Installation script finished"